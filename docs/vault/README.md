# Vault Secrets Management

* [Vault Secrets Management](vault.md)
* [How to use Vault for Secrets Management in Infrastructure](usage.md).
* [Vault Administration](administration.md).

<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

# Vault Service

* [Service Overview](https://dashboards.gitlab.net/d/vault-main/vault-overview)
* **Alerts**: <https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22vault%22%2C%20tier%3D%22inf%22%7D>
* **Label**: gitlab-com/gl-infra/production~"Service:Vault"

## Logging

* [Vault](https://nonprod-log.gitlab.net/goto/331c9c10-21df-11ed-af31-918941b0065a)

## Troubleshooting Pointers

* [Cloudflare for the on-call](../cloudflare/oncall.md)
* [VM Build Process with Terraform and Chef](../config_management/chef-process-overview.md)
* [Chef Vault Basics](../config_management/chef-vault.md)
* [Chef Tips and Tools](../config_management/chef-workflow.md)
* [customers.gitlab.com](../customersdot/api-key-rotation.md)
* [../elastic/elastic-cloud.md](../elastic/elastic-cloud.md)
* [HAProxy management at GitLab](../frontend/haproxy.md)
* [Gitaly token rotation](../gitaly/gitaly-token-rotation.md)
* [StatefulSet Guidelines](../kube/sts-guidelines.md)
* [GPG Keys for Package Signing](../packaging/manage-package-signing-keys.md)
* [Credential rotation](../patroni/postgresql-role-credential-rotation.md)
* [Rotating Rails' PostgreSQL password](../patroni/rotating-rails-postgresql-password.md)
* [Praefect Database](../praefect/praefect-database.md)
* [Add and remove file storages to praefect](../praefect/praefect-file-storages.md)
* [Container Registry CDN](../registry/cdn.md)
* [../uncategorized/about-gitlab-com.md](../uncategorized/about-gitlab-com.md)
* [Aptly](../uncategorized/aptly.md)
* [Chef secrets using GKMS](../uncategorized/gkms-chef-secrets.md)
* [Managing Chef](../uncategorized/manage-chef.md)
* [PackageCloud Infrastructure and Backups](../uncategorized/packagecloud-infrastructure.md)
* [Renaming Nodes](../uncategorized/rename-nodes.md)
* [Reprovisioning nodes](../uncategorized/reprovisioning-nodes.md)
* [Shared Configurations](../uncategorized/shared-configurations.md)
* [../uncategorized/subnet-allocations.md](../uncategorized/subnet-allocations.md)
* [Configuring and Using the Yubikey](../uncategorized/yubikey.md)
* [Vault Administration](administration.md)
* [Troubleshooting Hashicorp Vault](troubleshooting.md)
* [How to Use Vault for Secrets Management in Infrastructure](usage.md)
* [Vault Secrets Management](vault.md)
<!-- END_MARKER -->
